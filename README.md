# Handbook Finder

Handbook Finder is a web application that allows users to search for various items from the GM Handbook. It's built with Vite, React and TypeScript, and uses Tailwind CSS for styling.

## Features

-   Search functionality for various items
-   Copy ID functionality for each item
-   Dark mode for better user experience
-   Responsive design

## Getting Started

To get a local copy up and running, follow these steps:

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/YuukiPS/handbook.git
    ```

2. Install the dependencies:

    ```bash
    yarn install or npm install
    ```

3. Start the development server:

    ```bash
    yarn dev or npm run dev
    ```

The application will be available at `http://localhost:5173`.

## Deployment

The project is configured to be deployed on GitLab Pages. The configuration can be found in the [.gitlab-ci.yml](./.gitlab-ci.yml) file.

## Contributing

Contributions are welcome. Please open an issue first to discuss what you would like to change.

## License

This project is licensed under the MIT License. See the [LICENSE](./LICENSE) file for more details.

## Acknowledgements

-   This project uses the API from [api.elaxan.com](https://api.elaxan.com)
