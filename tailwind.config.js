/** @type {import('tailwindcss').Config} */
export default {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
        extend: {
            transitionProperty: {
                height: 'height',
                'm-height': 'max-height',
            },
        },
    },
    daisyui: {
        themes: ['night'],
    },
    corePlugins: {},
    plugins: [require('daisyui')],
};
