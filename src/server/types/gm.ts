export type CommandCategory =
    | 'Avatars'
    | 'Artifacts'
    | 'Monsters'
    | 'Materials'
    | 'Achievements'
    | 'Quests'
    | 'Scenes'
    | 'Dungeons'
    | 'Weapons';

export interface Command {
    gc: {
        [key: string]: {
            name: string;
            value: string;
        };
    };
    gio: {
        [key: string]: {
            name: string;
            value: string;
        };
    };
}

export type Language = {
    [key: string]: string;
};
export interface ListLanguage {
    [key: string]: string;
}

export interface ImageCharacter {
    card: string;
    icon: string;
    side: string;
}

export interface List {
    id: number;
    command: Command;
}

export interface AvatarsList extends List {
    rarity: number;
    category: 'Avatars';
    image:
        | {
              card: string;
              icon: string;
              side: string;
          }
        | undefined;
}

export interface AvatarsListV1 extends AvatarsList {
    name: string;
    description?: string;
}

export interface AvatarsListV2 extends AvatarsList {
    name: Language;
    description?: Language;
}

export interface ArtifactsList extends List {
    rarity: number;
    image?: string;
    category: 'Artifacts';
}

export interface ArtifactsListV1 extends ArtifactsList {
    name: string;
    description?: string;
}

export interface ArtifactsListV2 extends ArtifactsList {
    name: Language;
    description?: Language;
}

export interface MonstersList extends List {
    category: 'Monsters';
    image?: string;
}

export interface MonstersListV1 extends MonstersList {
    name: string;
    description: string;
}

export interface MonstersListV2 extends MonstersList {
    name: Language;
    description?: string;
}

export interface MaterialsList extends List {
    category: 'Materials';
    rarity?: number;
    image?: string;
}

export interface MaterialsListV1 extends MaterialsList {
    name: string;
    description?: string;
}

export interface MaterialsListV2 extends MaterialsList {
    name: Language;
    description?: Language;
}

export interface AchievementsList extends List {
    category: 'Achievements';
}

export interface AchievementsListV1 extends AchievementsList {
    name: string;
    description?: string;
}

export interface AchievementsListV2 extends AchievementsList {
    name: Language;
    description: Language;
}

export interface QuestsList extends List {
    category: 'Quests';
    image: string | undefined;
}

export interface QuestsListV1 extends QuestsList {
    name: string;
}

export interface QuestsListV2 extends QuestsList {
    name: Language;
    description?: Language;
}

export interface ScenesList extends List {
    category: 'Scenes';
    type: 'Dungeon' | 'World' | 'Room' | 'Home';
}

export interface ScenesListV1 extends ScenesList {
    name: string;
}

export interface ScenesListV2 extends ScenesList {
    name: Language;
}

export interface DungeonsList extends List {
    category: 'Dungeons';
}

export interface DungeonsListV1 extends DungeonsList {
    name: string;
}

export interface DungeonsListV2 extends DungeonsList {
    name: Language;
    description?: Language;
}

export interface WeaponsList extends List {
    rarity: number;
    image: string;
    category: 'Weapons';
}

export interface WeaponsListV1 extends WeaponsList {
    name: string;
    description?: string;
}

export interface WeaponsListV2 extends WeaponsList {
    name: Language;
    description?: Language | undefined;
}

export interface MainData {
    name: string;
    description: string;
    author: string;
    data: (
        | AvatarsListV2
        | ArtifactsListV2
        | MonstersListV2
        | MaterialsListV2
        | AchievementsListV2
        | QuestsListV2
        | ScenesListV2
        | DungeonsListV2
        | WeaponsListV2
    )[];
}

export type Find =
    | AvatarsListV2
    | ArtifactsListV2
    | MonstersListV2
    | MaterialsListV2
    | AchievementsListV2
    | QuestsListV2
    | ScenesListV2
    | DungeonsListV2
    | WeaponsListV2;

export interface AutocompleteResponse {
    name: string;
    value: string;
}
