import axios, { AxiosError } from 'axios';

export interface Account {
    data: Account_Data;
    message: string;
    retcode: number;
}

export interface Account_Data {
    account: Account;
    player: Player[];
}

export interface Account {
    uid: string;
    locale: string;
    code: string;
}

export interface Player {
    data: PlayerData;
    name: string;
    title: string;
    type_game: number;
    api_game: number;
}

export interface PlayerData {
    message: string;
    retcode: number;
    data: DataData | null;
}

export interface DataData {
    _id: number;
    accountId: string;
    nickname: string;
    signature: string;
}

export interface returnApplyTheCommand {
    message: string;
    retcode: number;
}

const timeoutInSeconds = 30;

const removeLeadingSymbol = (input: string): string => {
    const symbolRegex = /^[^A-Za-z0-9]+/;
    const match = input.match(symbolRegex);

    if (match && match.index === 0) {
        return input.slice(match[0].length);
    }

    return input;
};

const generateErrorMessage = (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    error: any,
    isAxiosError: boolean,
): { message: string; retcode: number } => {
    if (isAxiosError) {
        const { response } = error as AxiosError;
        const errorMessage = response
            ? `There was an error while sending a request command to <https://ps.yuuki.me/command> ${response.statusText} | ${response.status}`
            : `There was an Axios error: ${error.message}`;
        return { message: errorMessage, retcode: response?.status || 502 };
    } else {
        return { message: `There was an error\n${error}`, retcode: 500 };
    }
};

export default class YuukiPS {
    public static apiUrlAccount = 'https://ps.yuuki.me/api/v2/account';

    public static apiUrlCommand =
        'https://ps.yuuki.me/api/v2/server/:server/command';

    /**
     * Apply command to YuukiPS API
     *
     * @param {string} uid - UID of the player
     * @param {string | number} code - Code of the player
     * @param {string} server - Server of the player
     * @param {string} cmd - Command to apply
     * @returns {Promise<returnApplyTheCommand>} Result of the command
     */
    public static async applyCommand(
        uid: string,
        code: string | number,
        server: string,
        cmd: string,
    ): Promise<returnApplyTheCommand> {
        if (!uid || !server || !cmd) {
            return { message: 'Invalid input parameters', retcode: 400 };
        }

        const command = removeLeadingSymbol(cmd);
        const apiUrl = `${this.apiUrlCommand}`.replace(':server', server);
        const requestBody = {
            uid,
            server,
            code,
            cmd: command,
        };
        try {
            const resultFromAPI = await axios
                .get<returnApplyTheCommand>(apiUrl, {
                    params: requestBody,
                    timeout: 1000 * timeoutInSeconds,
                })
                .then((response) => response.data);

            if (resultFromAPI) {
                return resultFromAPI;
            } else {
                return { message: "Can't Access the server", retcode: 404 };
            }
        } catch (error) {
            return generateErrorMessage(error, axios.isAxiosError(error));
        }
    }

    /**
     * Check account information
     *
     * @param {string} uid - UID of the player
     * @param {string | number} code - Code of the player
     * @returns {Promise<Player[]>} Array of player data
     */
    public static async checkAccount(
        uid: string,
        code: string,
    ): Promise<Player[]> {
        const account = await axios
            .post<Account>(this.apiUrlAccount, {
                uid,
                code,
            })
            .then((response) => response.data);

        if (!account.data) {
            throw new Error(
                `Failed to get account information [${account.message}]`,
            );
        }

        return account.data.player.filter((item) => {
            if (item.data?.data) {
                return item;
            }
        });
    }

    /**
     * Extract formatted placeholders
     * @param {string} command - Command to apply
     * @returns {string[]} Array of formatted placeholders
     */
    public static extractFormattedPlaceholders(command: string): string[] {
        const placeholders = command.match(/<[^>]+>/g) || [];
        return placeholders.map((ph) => {
            const rawPlaceholder = ph.slice(1, -1);
            return rawPlaceholder.replace(/([A-Z])/g, ' $1').trim();
        });
    }

    /**
     * Generate result command
     * @param {string} command - Command to apply
     * @param {object} values - Values to replace
     * @returns {string} Result of the command
     */
    public static generateResultCommand(
        command: string,
        values: { [key: string]: string },
    ): string {
        let resultCommand = command;
        const placeholders = command.match(/<[^>]+>/g) || [];

        placeholders.forEach((ph) => {
            const key = ph.slice(1, -1).replace(/ /g, '');
            if (values[key] !== undefined) {
                resultCommand = resultCommand.replace(ph, values[key]);
            } else {
                const placeholderPattern = new RegExp(`[a-zA-Z]*${ph}`, 'g');
                resultCommand = resultCommand.replace(placeholderPattern, '');
            }
        });

        return resultCommand.trim();
    }
}
