import axios from 'axios';
import { MainData } from '@server/types/gm';

export default class GMHandbookAPI {
    public static readonly apiUrl = 'https://api.elaxan.com/v2/gm';

    public static async getGMHandbook(
        search: string = '',
        language?: string,
    ): Promise<MainData> {
        try {
            const response = await axios.post<MainData>(
                this.apiUrl,
                {
                    search,
                    type: 1,
                    language,
                },
                {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                },
            );
            return response.data;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }
}
