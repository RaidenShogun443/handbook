import axios from 'axios';
import { Hsr } from '@server/types/hsr';

async function getHSRData(
    search: string = '',
    language?: string,
): Promise<Hsr> {
    const response = await axios.post<Hsr>('https://api.elaxan.com/v1/sr', {
        search,
        type: 1,
        language,
    });
    return response.data;
}

export default getHSRData;
