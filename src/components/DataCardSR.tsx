import React from 'react';
import { Datum, ImageClass } from '@server/types/hsr';
import { FaStar } from 'react-icons/fa';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { toast } from 'react-toastify';

interface DataCardSRProps {
    data: Datum[];
    currentLanguage: 'en' | 'id' | 'ru' | 'jp' | 'th' | 'chs' | 'cht' | 'fr';
    currentLimit: number;
    showImage: boolean;
}

const DataCardSR: React.FC<DataCardSRProps> = ({
    data,
    currentLanguage,
    currentLimit,
    showImage,
}) => {
    const handleImage = (image: string | ImageClass): string => {
        if (typeof image === 'string') {
            return image;
        } else if (typeof image === 'object') {
            return image.icon;
        } else {
            return '';
        }
    };

    const getImageComponent = (item: Datum) => {
        const defaultImage =
            'https://api.elaxan.com/images/genshin-impact/not-found.png';
        const imageSrc = handleImage(item.image || '') || defaultImage;

        return (
            <div className='flex-shrink-0'>
                <LazyLoadImage
                    src={imageSrc}
                    alt={item.name[currentLanguage]}
                    className='w-32 rounded-lg max-sm:ml-3 sm:ml-3 md:ml-0'
                    effect='opacity'
                    onError={(e) => {
                        e.currentTarget.style.display = 'none';
                    }}
                />
            </div>
        );
    };

    const handleButtonCopy = (
        e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    ) => {
        const { value } = e.currentTarget;
        const [name, id] = value.split(' || ');
        const clipboard = navigator.clipboard;
        if (!clipboard) {
            toast.error('Clipboard API is not available', {
                position: 'bottom-right',
            });
            return;
        }
        clipboard
            .writeText(id)
            .then(() => {
                toast.info(`Copied ID ${id} for ${name}`, {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    rtl: true,
                });
            })
            .catch((error) => {
                console.error('Failed to copy:', error);
                toast.error('Failed to copy ID', {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    rtl: true,
                });
            });
    };

    return (
        <>
            {data.slice(0, currentLimit).map((item, index) => (
                <div
                    className='mt-5 flex items-center justify-center'
                    key={index}
                >
                    <div className='w-full bg-slate-800 md:max-w-md lg:max-w-lg xl:max-w-xl'>
                        <div className='flex flex-col rounded-lg p-5 shadow-lg md:flex-row'>
                            {showImage && getImageComponent(item)}
                            <div className='ml-3 flex flex-grow flex-col justify-between'>
                                <div>
                                    <div className='flex items-center'>
                                        <FaStar className='text-yellow-400' />
                                        <p className='ml-2 mt-[1px] font-bold text-yellow-400'>
                                            {item.rarity}
                                        </p>
                                    </div>
                                    <h1 className='text-2xl font-semibold'>
                                        {item.name[currentLanguage]}
                                    </h1>
                                    <p className='font-bold text-gray-600'>
                                        {item.id}
                                    </p>
                                    {item.description && (
                                        <p className='text-gray-500'>
                                            {item.description[currentLanguage]}
                                        </p>
                                    )}
                                </div>
                                <div className='mt-4 flex items-end justify-between md:mt-0'>
                                    <div className='text-lg font-bold text-gray-300'>
                                        {item.category}
                                    </div>
                                    <button
                                        className='mt-2 rounded-lg bg-blue-600 px-4 py-2 text-white hover:bg-blue-700'
                                        value={`${item.name[currentLanguage]} || ${item.id}`}
                                        onClick={handleButtonCopy}
                                    >
                                        Copy ID
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </>
    );
};

export default DataCardSR;
