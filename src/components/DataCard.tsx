import React, { useState } from 'react';
import { FaStar } from 'react-icons/fa';
import { MdOutlineContentCopy } from 'react-icons/md';
import { Find } from '@server/types/gm';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { RiSlashCommands2 } from 'react-icons/ri';
import { toast } from 'react-toastify';
import YuukiPS from '@server/api/yuukips';

interface DataCardProps {
    filteredData: Find[];
    currentLanguage: string;
    currentLimit: number;
    showImage: boolean;
    showCommands: boolean;
    uid: string;
    code: string;
    server: string;
}

const DataCard: React.FC<DataCardProps> = ({
    filteredData,
    currentLanguage,
    currentLimit,
    showImage,
    showCommands,
    code,
    uid,
    server,
}) => {
    const [args, setArgs] = useState<string[]>([]);
    const [command, setCommand] = useState<string>('');

    const [output, setOutput] = useState<string[]>([]);
    const [activeTab, setActiveTab] = useState(
        server ? (server.includes('gc') ? 'gc' : 'gio') : 'gc',
    );

    const getImageComponent = (data: Find) => {
        const defaultImage =
            'https://api.elaxan.com/images/genshin-impact/not-found.png';
        let imageSrc = defaultImage;
        let isAvatar = false;

        if (data.category === 'Avatars' && data.image) {
            imageSrc = data.image.icon;
            isAvatar = true;
        } else if (data.category !== 'Avatars' && 'image' in data) {
            imageSrc = data.image || defaultImage;
        }

        return (
            <div className='flex-shrink-0'>
                <LazyLoadImage
                    src={imageSrc}
                    alt={isAvatar ? '' : 'Not Found'}
                    className={`w-32 rounded-lg max-sm:ml-3 sm:ml-3 md:ml-0 ${
                        'rarity' in data && data.rarity
                            ? `${convertNumberToText(data.rarity) || ''}`
                            : ''
                    }`}
                    effect='opacity'
                    onError={(e) => {
                        e.currentTarget.style.display = 'none';
                    }}
                />
            </div>
        );
    };

    const handleButtonCopy = (
        e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    ) => {
        const { value } = e.currentTarget;
        const [name, id] = value.split(' || ');
        const clipboard = navigator.clipboard;
        if (!clipboard) {
            toast.error('Clipboard API is not available', {
                position: 'bottom-right',
            });
            return;
        }
        clipboard
            .writeText(id)
            .then(() => {
                toast.info(`Copied ID ${id} for ${name}`, {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    rtl: true,
                });
            })
            .catch((error) => {
                console.error('Failed to copy:', error);
                toast.error('Failed to copy ID', {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    rtl: true,
                });
            });
    };

    const convertNumberToText = (num: number): string | undefined => {
        switch (num) {
            case 5:
                return 'bg-rarityFive';
            case 4:
                return 'bg-rarityFour';
            case 3:
                return 'bg-rarityThree';
            default:
                return undefined;
        }
    };

    const commandList = (data: Find, type: 'gc' | 'gio') => {
        return (
            <div className='tab-content rounded-box border-base-300 bg-base-100 p-6'>
                {Object.entries(data.command[type]).map(([key, value]) => (
                    <div className='p-2' key={key}>
                        <h3 className='font-bold'>{value.name}</h3>
                        <div className='mt-2 flex items-center justify-between rounded-lg bg-gray-700 p-2 outline-none focus:outline-none active:outline-none'>
                            <div className='group flex w-full items-center justify-between'>
                                <code>{value.value}</code>
                                <div className='flex items-center'>
                                    <div className='mr-2 cursor-pointer rounded-lg border-2 border-gray-600 p-2 transition-opacity duration-300 group-hover:opacity-100 md:opacity-0 lg:opacity-0'>
                                        <RiSlashCommands2
                                            onClick={() => {
                                                if (!uid) {
                                                    toast.error(
                                                        'You must enter your UID first.',
                                                        {
                                                            position:
                                                                'bottom-right',
                                                        },
                                                    );
                                                    return;
                                                }

                                                if (!code) {
                                                    toast.error(
                                                        'You must enter your code first.',
                                                        {
                                                            position:
                                                                'bottom-right',
                                                        },
                                                    );
                                                    return;
                                                }
                                                if (!server) {
                                                    toast.error(
                                                        'You must enter your server first.',
                                                        {
                                                            position:
                                                                'bottom-right',
                                                        },
                                                    );
                                                    return;
                                                }
                                                const formatCommand =
                                                    YuukiPS.extractFormattedPlaceholders(
                                                        value.value,
                                                    );
                                                setArgs(formatCommand);
                                                setCommand(value.value);
                                                (
                                                    document.getElementById(
                                                        'apply-the-command',
                                                    ) as HTMLDialogElement
                                                ).showModal();
                                            }}
                                        />
                                    </div>
                                    <div className='cursor-pointer rounded-lg border-2 border-gray-600 p-2 transition-opacity duration-300 group-hover:opacity-100 md:opacity-0 lg:opacity-0'>
                                        <MdOutlineContentCopy
                                            onClick={() => {
                                                navigator.clipboard.writeText(
                                                    value.value,
                                                );
                                                toast.info(
                                                    'The command has been copied.',
                                                    {
                                                        position:
                                                            'bottom-right',
                                                    },
                                                );
                                            }}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        );
    };

    return (
        <>
            {filteredData.slice(0, currentLimit).map((data, index) => (
                <div
                    className='mt-5 flex items-center justify-center'
                    key={index}
                >
                    <div className='w-full bg-slate-800 md:max-w-md lg:max-w-lg xl:max-w-xl'>
                        <div className='flex flex-col rounded-lg p-5 shadow-lg md:flex-row'>
                            {showImage && getImageComponent(data)}
                            <div className='ml-3 flex flex-grow flex-col justify-between'>
                                <div>
                                    {'rarity' in data && (
                                        <div className='flex items-center'>
                                            <FaStar className='text-yellow-400' />
                                            <p className='ml-2 mt-[1px] font-bold text-yellow-400'>
                                                {data.rarity}
                                            </p>
                                        </div>
                                    )}
                                    <h1 className='text-2xl font-semibold'>
                                        {data.name[currentLanguage]}
                                    </h1>
                                    <p className='font-bold text-gray-600'>
                                        {data.id}
                                    </p>
                                    <p className='text-gray-500'>
                                        {'description' in data &&
                                            (typeof data.description ===
                                            'object'
                                                ? data.description[
                                                      currentLanguage
                                                  ]
                                                : data.description)}
                                    </p>
                                </div>
                                <div className='mt-4 flex items-end justify-between md:mt-0'>
                                    <div className='text-lg font-bold text-gray-300'>
                                        {data.category}
                                    </div>
                                    <button
                                        className='mt-2 rounded-lg bg-blue-600 px-4 py-2 text-white hover:bg-blue-700'
                                        value={`${data.name[currentLanguage]} || ${data.id}`}
                                        onClick={handleButtonCopy}
                                    >
                                        Copy ID
                                    </button>
                                </div>
                            </div>
                        </div>
                        {showCommands && (
                            <div className={`bg-slate-800`}>
                                <div className={`collapse w-full`}>
                                    <input type='checkbox' />
                                    <div className='collapse-title flex justify-center px-4 py-2'>
                                        Show The Commands
                                    </div>
                                    <div className='collapse-content'>
                                        <div className='tabs-boxed tabs'>
                                            <input
                                                type='radio'
                                                role='tab'
                                                className={`tab ${
                                                    activeTab === 'gc' &&
                                                    'tab-active'
                                                }`}
                                                aria-label='GC'
                                                name='tabset'
                                                id='tab1'
                                                onClick={() => {
                                                    setActiveTab('gc');
                                                }}
                                            />
                                            {commandList(data, 'gc')}
                                            <input
                                                type='radio'
                                                role='tab'
                                                className={`tab ${
                                                    activeTab === 'gio' &&
                                                    'tab-active'
                                                }`}
                                                aria-label='GIO'
                                                name='tabset'
                                                id='tab2'
                                                onClick={() => {
                                                    setActiveTab('gio');
                                                }}
                                            />
                                            {commandList(data, 'gio')}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                        <dialog id='apply-the-command' className='modal'>
                            <div className='modal-box flex flex-col'>
                                <form method='dialog'>
                                    <button className='btn btn-sm absolute right-2 top-2 hover:bg-transparent'>
                                        x
                                    </button>
                                </form>
                                <h3 className='text-lg font-bold'>
                                    Apply the Command
                                </h3>
                                {args.map((arg, index) => (
                                    <>
                                        <label
                                            className='form-control'
                                            key={`apply-the-command-args-${index}`}
                                        >
                                            <span className='label-text'>
                                                {arg}
                                            </span>
                                            <input
                                                type='text'
                                                className='input input-bordered w-full'
                                                id={`data-card-apply-command-args-${arg.replace(
                                                    / /,
                                                    '',
                                                )}`}
                                            />
                                        </label>
                                    </>
                                ))}
                                {output.length > 0 && (
                                    <>
                                        <div className='output-container mt-5 max-h-[200px] overflow-y-auto bg-slate-950 p-2'>
                                            {output.map((text, index) => (
                                                <div
                                                    key={index}
                                                    className='text-sm'
                                                    dangerouslySetInnerHTML={{
                                                        __html: text,
                                                    }}
                                                ></div>
                                            ))}
                                        </div>
                                        <div className='mt-2 text-sm'>
                                            <button
                                                className='btn btn-ghost btn-sm'
                                                onClick={() => {
                                                    setOutput([]);
                                                }}
                                            >
                                                Clear output
                                            </button>
                                        </div>
                                    </>
                                )}

                                <button
                                    className='btn btn-primary mt-3'
                                    id='data-card-apply-command-button-apply'
                                    onClick={(e) => {
                                        e.currentTarget.disabled = true;
                                        const userValues = args.reduce<
                                            Record<string, string>
                                        >((obj, arg) => {
                                            const key = arg.replace(/ /g, '');
                                            const element =
                                                document.getElementById(
                                                    `data-card-apply-command-args-${key}`,
                                                ) as HTMLInputElement;
                                            if (
                                                element &&
                                                element.value !== ''
                                            ) {
                                                obj[key] = element.value;
                                            }
                                            return obj;
                                        }, {});
                                        const generateCommand =
                                            YuukiPS.generateResultCommand(
                                                command,
                                                userValues,
                                            );
                                        setOutput((prevOutput) => [
                                            ...prevOutput,
                                            `Executing command: ${generateCommand}`,
                                        ]);
                                        YuukiPS.applyCommand(
                                            uid,
                                            code,
                                            server,
                                            generateCommand,
                                        )
                                            .then((result) => {
                                                setOutput((prevOutput) => [
                                                    ...prevOutput,
                                                    `Response: ${result.message} | ${result.retcode}`,
                                                ]);
                                                (
                                                    document.getElementById(
                                                        'data-card-apply-command-button-apply',
                                                    ) as HTMLButtonElement
                                                ).disabled = false;
                                            })
                                            .catch((err) => {
                                                setOutput((prevOutput) => [
                                                    ...prevOutput,
                                                    `Error: ${err}`,
                                                ]);
                                                (
                                                    document.getElementById(
                                                        'data-card-apply-command-button-apply',
                                                    ) as HTMLButtonElement
                                                ).disabled = false;
                                            });
                                    }}
                                >
                                    Apply
                                </button>
                            </div>
                        </dialog>
                    </div>
                </div>
            ))}
        </>
    );
};

export default DataCard;
