import React, { useState } from 'react';
import { useCookies } from 'react-cookie';
import { IoMdSearch } from 'react-icons/io';
import { FaUserGear } from 'react-icons/fa6';
import YuukiPS, { Player } from '@server/api/yuukips';

interface SearchProps {
    listCategory: string[];
    selectedCategory: string;
    setSelectedCategory: React.Dispatch<React.SetStateAction<string>>;
    currentLanguage: string;
    searchTerm: string;
    setSearchTerm: React.Dispatch<React.SetStateAction<string>>;
    setShowImage: React.Dispatch<React.SetStateAction<boolean>>;
    setShowCommands: React.Dispatch<React.SetStateAction<boolean>>;
    currentType: 'Genshin Impact' | 'Star Rail';
    setCurrentType: React.Dispatch<
        React.SetStateAction<'Genshin Impact' | 'Star Rail'>
    >;
    handleSearch: React.Dispatch<React.SetStateAction<string>>;
    fetchOnSearch: boolean;
    setFetchOnSearch: React.Dispatch<React.SetStateAction<boolean>>;
    showImage: boolean;
    loadGI: () => Promise<void>;
    loadSR: () => Promise<void>;
    showCommands: boolean;
    setLoading: React.Dispatch<React.SetStateAction<boolean>>;
}

function expiresInAMonth(): Date {
    const oneMonthFromNow = new Date();
    oneMonthFromNow.setMonth(oneMonthFromNow.getMonth() + 1);
    return oneMonthFromNow;
}

const Search: React.FC<SearchProps> = ({
    listCategory,
    currentLanguage,
    selectedCategory,
    setSelectedCategory,
    searchTerm,
    setSearchTerm,
    handleSearch,
    currentType,
    setCurrentType,
    fetchOnSearch,
    setFetchOnSearch,
    loadGI,
    loadSR,
    setLoading,
    showImage,
    setShowImage,
    showCommands,
    setShowCommands,
}) => {
    const [cookie, setCookie] = useCookies([
        'language',
        'showImage',
        'showCommands',
        'type',
        'fetchOnSearch',
        'uid',
        'code',
        'server',
    ]);
    const [isOpen, setIsOpen] = useState(false);
    const [successCheckPlayer, setSuccessCheckPlayer] = useState(false);
    const [output, setOutput] = useState<string[]>([]);
    const [uid, setUid] = useState(cookie.uid || '');
    const [code, setCode] = useState(cookie.code || '');
    const [listServer, setListServer] = useState<Player[]>([]);

    const setOutputText = (text: string, color: string) => {
        setOutput([
            ...output,
            `<span class="text-${color}-500">${text}</span>`,
        ]);
    };

    return (
        <div className='mt-5 flex justify-center'>
            <div
                className={`relative flex w-full flex-col items-center justify-between rounded-lg border border-gray-600 shadow-lg md:w-3/4 lg:w-3/5`}
            >
                <h2 className='absolute left-5 top-9 text-gray-400 max-sm:hidden'>
                    {currentType}
                </h2>
                <div className='form-control w-full items-center justify-between p-4'>
                    <div className='label'>
                        <span className='label-text'>
                            <kbd className='kbd  mr-1'>Enter</kbd> to search
                        </span>
                        <span className='label-text ml-3'>
                            <kbd className='kbd '>Esc</kbd> to clear
                        </span>
                    </div>
                    <div className='flex w-full'>
                        <input
                            type='text'
                            placeholder='Search'
                            className='w-full rounded-l-lg border-2 border-gray-500 bg-transparent px-4 py-2 text-white outline-none'
                            value={searchTerm}
                            onChange={(e) => setSearchTerm(e.target.value)}
                            onBlur={() => {
                                handleSearch(searchTerm);
                                if (fetchOnSearch) {
                                    if (searchTerm === '') {
                                        return;
                                    }
                                    setLoading(true);
                                    if (currentType === 'Genshin Impact') {
                                        loadGI();
                                    } else if (currentType === 'Star Rail') {
                                        loadSR();
                                    }
                                }
                            }}
                            onKeyDown={(e) => {
                                if (e.key === 'Enter') {
                                    handleSearch(searchTerm);
                                    if (fetchOnSearch) {
                                        if (searchTerm === '') {
                                            return;
                                        }
                                        setLoading(true);
                                        if (currentType === 'Genshin Impact') {
                                            loadGI();
                                        } else if (
                                            currentType === 'Star Rail'
                                        ) {
                                            loadSR();
                                        }
                                    }
                                } else if (e.key === 'Escape') {
                                    setSearchTerm('');
                                    handleSearch('');
                                }
                            }}
                        />
                        <IoMdSearch
                            className='cursor-pointer rounded-r-lg bg-primary px-3 text-white'
                            size={50}
                            onClick={() => {
                                handleSearch(searchTerm);
                                if (fetchOnSearch) {
                                    setLoading(true);
                                    if (currentType === 'Genshin Impact') {
                                        loadGI();
                                    } else if (currentType === 'Star Rail') {
                                        loadSR();
                                    }
                                }
                            }}
                        />
                    </div>
                    <div className='label'>
                        <label className='btn swap label-text-alt swap-rotate scale-90'>
                            {/* this hidden checkbox controls the state */}
                            <input
                                type='checkbox'
                                onClick={() => {
                                    setIsOpen(!isOpen);
                                }}
                            />

                            {/* hamburger icon */}
                            <svg
                                className={`swap-off fill-current`}
                                xmlns='http://www.w3.org/2000/svg'
                                width='25'
                                height='25'
                                viewBox='0 0 512 512'
                            >
                                <path d='M64,384H448V341.33H64Zm0-106.67H448V234.67H64ZM64,128v42.67H448V128Z' />
                            </svg>

                            {/* close icon */}
                            <svg
                                className={`swap-on fill-current`}
                                xmlns='http://www.w3.org/2000/svg'
                                width='25'
                                height='25'
                                viewBox='0 0 512 512'
                            >
                                <polygon points='400 145.49 366.51 112 256 222.51 145.49 112 112 145.49 222.51 256 112 366.51 145.49 400 256 289.49 366.51 400 400 366.51 289.49 256 400 145.49' />
                            </svg>
                        </label>
                        <button
                            onClick={() => {
                                (
                                    document.getElementById(
                                        'modal-player-set',
                                    ) as HTMLDialogElement
                                )?.showModal();
                            }}
                            className='btn scale-90'
                        >
                            <FaUserGear size={20} />
                        </button>
                        <dialog id='modal-player-set' className='modal'>
                            <div className='modal-box flex flex-col'>
                                <form method='dialog'>
                                    <button className='btn btn-circle btn-ghost btn-lg absolute right-2 top-2 hover:bg-transparent'>
                                        x
                                    </button>
                                </form>
                                <h3 className='text-lg font-bold'>
                                    Player Settings
                                </h3>
                                <label className='form-control'>
                                    <div className='label'>
                                        <span className='label-text'>
                                            UID Account
                                        </span>
                                    </div>
                                    <input
                                        type='number'
                                        placeholder='UID'
                                        className='input input-bordered input-primary'
                                        id='player-set-uid'
                                        onKeyDown={(e) => {
                                            if (e.key === 'Enter') {
                                                (
                                                    document.getElementById(
                                                        'player-set-code',
                                                    ) as HTMLInputElement
                                                ).focus();
                                            }
                                        }}
                                    />
                                </label>
                                <label className='form-control'>
                                    <div className='label'>
                                        <span className='label-text'>Code</span>
                                    </div>
                                    <input
                                        type='password'
                                        placeholder='Code'
                                        className='input input-bordered input-primary'
                                        id='player-set-code'
                                        onKeyDown={(e) => {
                                            if (e.key === 'Enter') {
                                                (
                                                    document.getElementById(
                                                        'player-set-check',
                                                    ) as HTMLButtonElement
                                                ).click();
                                            }
                                        }}
                                    />
                                </label>
                                <div className='my-4'>
                                    <a
                                        href='https://ps.yuuki.me/account/home'
                                        target='_blank'
                                        rel='noreferrer'
                                    >
                                        Get UID and Code
                                    </a>
                                </div>
                                <button
                                    className='btn btn-primary'
                                    id='player-set-check'
                                    disabled={successCheckPlayer}
                                    onClick={() => {
                                        const uid = (
                                            document.getElementById(
                                                'player-set-uid',
                                            ) as HTMLInputElement
                                        ).value;
                                        const code = (
                                            document.getElementById(
                                                'player-set-code',
                                            ) as HTMLInputElement
                                        ).value;
                                        if (!uid || !code) {
                                            setOutputText(
                                                'UID or Code empty',
                                                'red',
                                            );
                                            return;
                                        }
                                        if (uid.length < 5) {
                                            setOutputText(
                                                'UID must be 5 digits',
                                                'red',
                                            );
                                            return;
                                        }

                                        if (code.length < 4) {
                                            setOutputText(
                                                'Code must be 5 digits',
                                                'red',
                                            );
                                            return;
                                        }
                                        YuukiPS.checkAccount(uid, code)
                                            .then((res) => {
                                                setSuccessCheckPlayer(true);
                                                setOutputText(
                                                    `Successfully checked UID ${uid}. There are ${res.length} accounts registered with this UID.`,
                                                    'green',
                                                );
                                                setListServer(res);
                                                setCode(code);
                                                setUid(uid);
                                            })
                                            .catch((reason) => {
                                                setOutputText(
                                                    `${reason}`,
                                                    'red',
                                                );
                                                setSuccessCheckPlayer(false);
                                            });
                                    }}
                                >
                                    Check
                                </button>
                                {successCheckPlayer &&
                                    listServer.length > 0 && (
                                        <div className='dropdown dropdown-hover mt-4'>
                                            <div
                                                tabIndex={0}
                                                role='button'
                                                className='btn btn-primary w-full'
                                            >
                                                Select Server
                                            </div>
                                            <ul
                                                tabIndex={0}
                                                className='menu dropdown-content z-[1] w-full rounded-box bg-gray-800 p-2 shadow'
                                            >
                                                {listServer.map(
                                                    (player, index) => (
                                                        <li
                                                            key={`list-server-${index}`}
                                                        >
                                                            <a
                                                                href='#'
                                                                onClick={(
                                                                    e,
                                                                ) => {
                                                                    e.preventDefault();

                                                                    setCookie(
                                                                        'server',
                                                                        player.name,
                                                                        {
                                                                            path: '/',
                                                                            expires:
                                                                                expiresInAMonth(),
                                                                        },
                                                                    );
                                                                    setCookie(
                                                                        'uid',
                                                                        player
                                                                            .data
                                                                            .data
                                                                            ?._id,
                                                                        {
                                                                            path: '/',
                                                                            expires:
                                                                                expiresInAMonth(),
                                                                        },
                                                                    );
                                                                    setUid(
                                                                        player
                                                                            .data
                                                                            .data
                                                                            ?._id,
                                                                    );
                                                                    setOutputText(
                                                                        `Server ${player.title} has been successfully saved.`,
                                                                        'green',
                                                                    );
                                                                    e.stopPropagation();
                                                                }}
                                                            >
                                                                {player.title}
                                                            </a>
                                                        </li>
                                                    ),
                                                )}
                                            </ul>
                                        </div>
                                    )}
                                {output.length > 0 && (
                                    <>
                                        <div className='output-container mt-5 max-h-[200px] overflow-y-auto bg-slate-950 p-2'>
                                            {output.map((text, index) => (
                                                <div
                                                    key={index}
                                                    className='text-sm'
                                                    dangerouslySetInnerHTML={{
                                                        __html: text,
                                                    }}
                                                ></div>
                                            ))}
                                        </div>
                                        <div className='mt-2 text-sm'>
                                            <button
                                                className='btn btn-ghost btn-sm'
                                                onClick={() => {
                                                    setOutput([]);
                                                    setSuccessCheckPlayer(
                                                        false,
                                                    );
                                                }}
                                            >
                                                Clear output
                                            </button>
                                        </div>
                                    </>
                                )}
                                {successCheckPlayer && (
                                    <div className='modal-action'>
                                        <button
                                            className='btn btn-primary'
                                            onClick={(e) => {
                                                e.preventDefault();

                                                setSuccessCheckPlayer(false);
                                                setCookie('code', code, {
                                                    path: '/',
                                                    expires: expiresInAMonth(),
                                                });
                                                setCookie('uid', uid, {
                                                    path: '/',
                                                    expires: expiresInAMonth(),
                                                });
                                                setOutputText(
                                                    'UID and Code have been successfully saved.',
                                                    'green',
                                                );
                                            }}
                                        >
                                            Save
                                        </button>
                                        <button
                                            className='btn'
                                            onClick={(e) => {
                                                e.preventDefault();
                                                setSuccessCheckPlayer(false);
                                                setOutputText(
                                                    'Canceled.',
                                                    'green',
                                                );
                                            }}
                                        >
                                            Cancel
                                        </button>
                                    </div>
                                )}
                            </div>
                        </dialog>
                    </div>
                </div>
                <div
                    className={`flex w-full flex-col overflow-hidden transition-m-height duration-300 ${
                        isOpen ? 'max-h-[250px]' : 'max-h-0'
                    }`}
                >
                    <select
                        value={currentType}
                        onChange={(e) => {
                            setCurrentType(
                                e.target.value as
                                    | 'Genshin Impact'
                                    | 'Star Rail',
                            );
                            setCookie('type', e.target.value, {
                                path: '/',
                                expires: expiresInAMonth(),
                            });
                            setSelectedCategory('category');
                            setSearchTerm('');
                            handleSearch('');
                        }}
                        className='select select-primary m-2 rounded-lg px-4 py-2 text-white focus:border-transparent  focus:outline-none focus:ring-2'
                    >
                        <option value={'Genshin Impact'}>Genshin Impact</option>
                        <option value={'Star Rail'}>Honkai: Star Rail</option>
                    </select>

                    <select
                        value={selectedCategory}
                        onChange={(e) => setSelectedCategory(e.target.value)}
                        className='select select-primary m-2 rounded-lg px-4 py-2 text-white focus:border-transparent  focus:outline-none focus:ring-2'
                    >
                        <>
                            <option
                                key={'default-category'}
                                value={'category'}
                                selected
                            >
                                Category
                            </option>
                            {listCategory.map((category, index) => (
                                <option key={index} value={category}>
                                    {category}
                                </option>
                            ))}
                        </>
                    </select>
                    <select
                        value={currentLanguage}
                        onChange={(e) => {
                            console.log(e.target.value);
                            setCookie('language', e.target.value, {
                                path: '/',
                                expires: expiresInAMonth(),
                            });
                        }}
                        className='select select-primary m-2 rounded-lg px-4 py-2 text-white focus:border-transparent  focus:outline-none focus:ring-2'
                    >
                        <option value={'jp'}>Japanese</option>
                        <option value={'fr'}>French</option>
                        <option value={'id'}>Indonesian</option>
                        <option value={'ru'}>Russian</option>
                        <option value={'en'}>English</option>
                        <option value={'chs'}>
                            Simplified Chinese (Mainland China)
                        </option>
                        <option value={'cht'}>
                            Traditional Chinese (Hong Kong)
                        </option>
                        <option value={'th'}>Thai</option>
                    </select>
                    <div className='flex items-center justify-center'>
                        <div className='form-control flex items-center justify-center'>
                            <label className='label cursor-pointer'>
                                <span className='label-text'>Show Image</span>
                                <input
                                    className='checkbox ml-1'
                                    type='checkbox'
                                    checked={showImage}
                                    onChange={(e) =>
                                        setShowImage(e.target.checked)
                                    }
                                />
                            </label>
                        </div>
                        {currentType === 'Genshin Impact' && (
                            <div className='form-control flex items-center justify-center'>
                                <label className='label cursor-pointer'>
                                    <span className='label-text'>
                                        Show Commands
                                    </span>
                                    <input
                                        className='checkbox ml-1'
                                        type='checkbox'
                                        checked={showCommands}
                                        onChange={(e) =>
                                            setShowCommands(e.target.checked)
                                        }
                                    />
                                </label>
                            </div>
                        )}
                        <div className='form-control'>
                            <label className='label cursor-pointer'>
                                <span className='label-text'>
                                    Fetch on Search
                                </span>
                                <input
                                    type='checkbox'
                                    className='toggle md:ml-2'
                                    checked={fetchOnSearch}
                                    onChange={(e) => {
                                        setFetchOnSearch(e.target.checked);
                                        setCookie(
                                            'fetchOnSearch',
                                            e.target.checked,
                                            {
                                                path: '/',
                                                expires: expiresInAMonth(),
                                            },
                                        );
                                    }}
                                />
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Search;
