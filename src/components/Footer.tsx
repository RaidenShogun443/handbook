import React from 'react';
import { FaGitlab, FaGithub, FaDiscord } from 'react-icons/fa6';
import { SiBuymeacoffee } from 'react-icons/si';
import { TbLicense } from 'react-icons/tb';
import { FiExternalLink } from 'react-icons/fi';

const Footer: React.FC = () => {
    return (
        <footer className='mt-auto w-full bg-gray-800 px-6 py-4 text-white'>
            <div className='footer'>
                <nav>
                    <header className='footer-title'>ElaXan</header>
                    <a
                        className='link-hover link'
                        href='https://discord.com/users/506212044152897546'
                    >
                        <FaDiscord className='-mt-1 mr-1 inline-block' />
                        Discord
                        <FiExternalLink className='-mt-1 ml-1 inline-block' />
                    </a>
                    <a
                        className='link-hover link'
                        href='https://github.com/ElaXan'
                    >
                        <FaGithub className='-mt-1 mr-1 inline-block' />
                        GitHub
                        <FiExternalLink className='-mt-1 ml-1 inline-block' />
                    </a>
                    <a
                        className='link-hover link'
                        href='https://gitlab.com/ElaXan'
                    >
                        <FaGitlab className='-mt-1 mr-1 inline-block' />
                        GitLab
                        <FiExternalLink className='-mt-1 ml-1 inline-block' />
                    </a>
                </nav>
                <nav>
                    <header className='footer-title'>Handbook Finder</header>
                    <a
                        className='link-hover link'
                        href='https://gitlab.com/YuukiPS/handbook'
                    >
                        <FaGitlab className='-mt-1 mr-1 inline-block' />
                        GitLab
                        <FiExternalLink className='-mt-1 ml-1 inline-block' />
                    </a>
                    <a
                        className='link-hover link'
                        href='https://gitlab.com/YuukiPS/handbook/-/blob/main/LICENSE'
                    >
                        <TbLicense className='-mt-1 mr-1 inline-block' />
                        License
                        <FiExternalLink className='-mt-1 ml-1 inline-block' />
                    </a>
                </nav>
                <nav>
                    <header className='footer-title'>Donation</header>
                    <a
                        className='link-hover link'
                        href='https://buymeacoffee.com/elashxander'
                    >
                        <SiBuymeacoffee className='-mt-1 mr-1 inline-block' />
                        BuyMeaCoffee
                        <FiExternalLink className='-mt-1 ml-1 inline-block' />
                    </a>
                    <a
                        className='link-hover link'
                        href='https://saweria.co/ElaXan'
                    >
                        Saweria
                        <FiExternalLink className='-mt-1 ml-1 inline-block' />
                    </a>
                </nav>
            </div>
            <aside className='flex justify-center'>
                <p>Copyright © {new Date().getFullYear()} ElaXan</p>
            </aside>
        </footer>
    );
};

export default Footer;
