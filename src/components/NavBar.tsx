import React, { useState } from 'react';
import { AiOutlineMenu, AiOutlineSearch } from 'react-icons/ai';

const Navbar: React.FC = () => {
    const [isMobileMenuOpen, setIsMobileMenuOpen] = useState<boolean>(false);

    const handleMobileMenu = () => {
        setIsMobileMenuOpen(!isMobileMenuOpen);
    };

    return (
        <div className='navbar mt-2 rounded-lg bg-gray-800'>
            <div className='h-16 flex-1 items-center justify-between px-2 sm:px-0'>
                <div className='ml-3 flex items-center'>
                    <a
                        href='/'
                        className='flex text-xl font-bold text-white no-underline'
                    >
                        <AiOutlineSearch className='mr-2' size={30} />
                        Handbook
                    </a>
                </div>
                <div className='mr-3 hidden sm:flex'>
                    <a
                        href='https://github.com/ElaXan'
                        className='rounded-md px-3 py-2 text-sm font-medium text-gray-300 no-underline hover:bg-gray-700 hover:text-white'
                    >
                        ElaXan
                    </a>
                    <a
                        href='https://discord.yuuki.me'
                        className='rounded-md px-3 py-2 text-sm font-medium text-gray-300 no-underline hover:bg-gray-700 hover:text-white'
                    >
                        Discord
                    </a>
                </div>
                <div className='mr-2 flex sm:hidden'>
                    <button
                        onClick={handleMobileMenu}
                        className='rounded-md px-3 py-2 text-sm font-medium text-gray-300 hover:text-white'
                    >
                        <AiOutlineMenu size={30} />
                    </button>
                </div>
            </div>
            {isMobileMenuOpen && (
                <div className='px-2 pb-3 pt-2 sm:hidden'>
                    <a
                        href='https://github.com/ElaXan'
                        className='block rounded-md bg-gray-900 px-3 py-2 text-base font-medium text-white'
                    >
                        ElaXan
                    </a>
                    <a
                        href='https://discord.yuuki.me'
                        className='mt-1 block rounded-md bg-gray-900 px-3 py-2 text-base font-medium text-white'
                    >
                        Discord
                    </a>
                </div>
            )}
        </div>
    );
};

export default Navbar;
