import React, { useState } from 'react';

interface PopoverProps {
    content: string;
    children: React.ReactNode
}

const Popover: React.FC<PopoverProps> = ({ content, children }) => {
    const [show, setShow] = useState(false);
    
    return (
        <div className="relative">
            <div 
                onMouseEnter={() => setShow(true)} 
                onMouseLeave={() => setShow(false)}
            >
                {children}
            </div>
            <div className={`absolute z-10 w-64 text-sm text-gray-500 transition-opacity duration-300 bg-white border border-gray-200 rounded-lg shadow-sm ${show ? 'opacity-100' : 'opacity-0'} dark:text-gray-400 dark:border-gray-600 dark:bg-gray-800 max-sm:right-0 sm:right-0 md:left-0`}>
                <div className="px-3 py-2 bg-gray-100 border-b border-gray-200 rounded-t-lg dark:border-gray-600 dark:bg-gray-700">
                    <h3 className="font-semibold text-gray-900 dark:text-white">Handbook Finder</h3>
                </div>
                <div className="px-3 py-2">
                    <p>{content}</p>
                </div>
            </div>
        </div>
    )
}

export default Popover