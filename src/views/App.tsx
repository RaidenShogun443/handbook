import React, { useEffect, useState, useMemo } from 'react';
import GM from '../server/api/gm';
import SR from '../server/api/sr';
import { Find } from '../server/types/gm';
import { useCookies } from 'react-cookie';
import { IoClose } from 'react-icons/io5';
import { ToastContainer } from 'react-toastify';
import Navbar from '@components/NavBar';
import Search from '@components/Search';
import DataCard from '@components/DataCard';
import DataCardSR from '@components/DataCardSR';
import Footer from '@components/Footer';
import 'react-toastify/dist/ReactToastify.css';
import { Datum } from '@server/types/hsr';

function App() {
    const [mainData, setMainData] = useState<Find[]>([]);
    const [mainDataSR, setMainDataSR] = useState<Datum[]>([]);
    const [searchTerm, setSearchTerm] = useState<string>('');
    const [searchInputValue, setSearchInputValue] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<string>();
    const [cookies, setCookie] = useCookies([
        'devMessage',
        'language',
        'showImage',
        'showCommands',
        'type',
        'fetchOnSearch',
        'code',
        'uid',
        'server',
    ]);
    const [listCategory, setListCategory] = useState<string[]>([]);
    const [currentType, setCurrentType] = useState<
        'Genshin Impact' | 'Star Rail'
    >(cookies.type || 'Genshin Impact');
    const currentLanguage:
        | 'en'
        | 'id'
        | 'ru'
        | 'jp'
        | 'th'
        | 'chs'
        | 'cht'
        | 'fr' = cookies.language || 'en';
    const [selectedCategory, setSelectedCategory] =
        useState<string>('category');
    const [showImage, setShowImage] = useState<boolean>(
        cookies.showImage !== undefined ? cookies.showImage : true,
    );
    const [showCommands, setShowCommands] = useState<boolean>(
        cookies.showCommands !== undefined ? cookies.showCommands : true,
    );
    const [fetchOnSearch, setFetchOnSearch] = useState<boolean>(
        cookies.fetchOnSearch !== undefined ? cookies.fetchOnSearch : false,
    );
    const [currentLimit, setCurrentLimit] = useState<number>(100);

    const loadGI = async () => {
        const time = Date.now();
        console.log('Called');
        console.log(`Retrieving a list of IDs from the API...`);
        try {
            const response = await GM.getGMHandbook(
                searchTerm,
                currentLanguage,
            );
            console.log(
                `Done, it took ${Date.now() - time}ms to fetch ${
                    response.data.length
                } data items`,
            );
            setLoading(false);
            setMainData(response.data);
            const categories = Array.from(
                new Set(response.data.map((item) => item.category)),
            ).map((name) => {
                return name;
            });
            setListCategory(categories);
        } catch (error) {
            setError(true);
            setLoading(false);
            if (error instanceof Error) {
                setErrorMessage(error.message);
            }
        }
    };

    const loadSR = async () => {
        const time = Date.now();
        console.log(`Retrieving a list of IDs from the API...`);
        try {
            const response = await SR(searchTerm, currentLanguage);
            console.log(
                `Done, it took ${Date.now() - time}ms to fetch ${
                    response.data.length
                } data items`,
            );
            setLoading(false);
            setMainDataSR(response.data);
            const categories = Array.from(
                new Set(response.data.map((item) => item.category)),
            ).map((name) => {
                return name;
            });
            setListCategory(categories);
        } catch (error) {
            setError(true);
            setLoading(false);
            if (error instanceof Error) {
                setErrorMessage(error.message);
            }
        }
    };

    useEffect(() => {
        console.log('Fetching data...');
        if (fetchOnSearch) {
            setLoading(false);
            return;
        }
        const loadGI = async () => {
            const time = Date.now();
            console.log(`Retrieving a list of IDs from the API...`);
            try {
                const response = await GM.getGMHandbook('', currentLanguage);
                console.log(
                    `Done, it took ${Date.now() - time}ms to fetch ${
                        response.data.length
                    } data items`,
                );
                setLoading(false);
                setMainData(response.data);
                const categories = Array.from(
                    new Set(response.data.map((item) => item.category)),
                ).map((name) => {
                    return name;
                });
                setListCategory(categories);
            } catch (error) {
                setError(true);
                setLoading(false);
                if (error instanceof Error) {
                    setErrorMessage(error.message);
                }
            }
        };

        const loadSR = async () => {
            const time = Date.now();
            console.log(`Retrieving a list of IDs from the API...`);
            try {
                const response = await SR('', currentLanguage);
                console.log(
                    `Done, it took ${Date.now() - time}ms to fetch ${
                        response.data.length
                    } data items`,
                );
                setLoading(false);
                setMainDataSR(response.data);
                const categories = Array.from(
                    new Set(response.data.map((item) => item.category)),
                ).map((name) => {
                    return name;
                });
                setListCategory(categories);
            } catch (error) {
                setError(true);
                setLoading(false);
                if (error instanceof Error) {
                    setErrorMessage(error.message);
                }
            }
        };

        if (currentType === 'Genshin Impact') {
            loadGI();
        } else if (currentType === 'Star Rail') {
            loadSR();
        }
    }, [currentType, fetchOnSearch, currentLanguage]);

    const filteredData = useMemo(() => {
        return mainData.filter((data) => {
            if (searchInputValue === '' && selectedCategory === null) {
                return data;
            } else if (
                data.name[currentLanguage]
                    ?.toLowerCase()
                    .includes(searchInputValue.toLowerCase()) &&
                (selectedCategory === null ||
                    selectedCategory === 'category' ||
                    data.category === selectedCategory)
            ) {
                return data;
            }
            return false;
        });
    }, [mainData, searchInputValue, selectedCategory, currentLanguage]);

    const filteredDataSR = useMemo(() => {
        return mainDataSR.filter((data) => {
            if (searchInputValue === '' && selectedCategory === null) {
                return data;
            } else if (
                data.name[currentLanguage]
                    ?.toLowerCase()
                    .includes(searchInputValue.toLowerCase()) &&
                (selectedCategory === null ||
                    selectedCategory === 'category' ||
                    data.category === selectedCategory)
            ) {
                return data;
            }
            return false;
        });
    }, [currentLanguage, mainDataSR, searchInputValue, selectedCategory]);

    const noResult =
        !loading &&
        (currentType === 'Genshin Impact' ? filteredData : filteredDataSR)
            .length === 0 &&
        searchInputValue !== '';

    return (
        <div
            className={`container mx-auto flex min-h-screen flex-col justify-between px-4 sm:px-6 lg:px-8 ${
                loading && 'cursor-progress'
            }`}
        >
            <Navbar />
            {/* Check if the devMessage cookie is not set */}
            {!cookies.devMessage && (
                <div className='flex justify-center'>
                    {/* Display a message box with a close button */}
                    <div className='relative mt-2 rounded-md border-4 border-red-600 bg-gray-800 p-3 text-center'>
                        <IoClose
                            className='absolute right-2 top-2 scale-125 cursor-pointer transition-transform duration-300 hover:scale-105'
                            onClick={(e: React.MouseEvent) => {
                                /* Set the devMessage cookie to 'closed' when the close button is clicked */
                                setCookie('devMessage', 'closed', {
                                    path: '/',
                                });
                                /* Hide the close button after it is clicked */
                                (e.currentTarget as HTMLElement).style.display =
                                    'none';
                            }}
                        />
                        <div className='pt-2'>
                            {/* Display a development message */}
                            <p className=''>
                                This is still under development, error may occur
                            </p>
                            {/* Display a message about the API source */}
                            <p className=''>
                                This tool uses the API from{' '}
                                <a
                                    href='https://api.elaxan.com'
                                    className='text-blue-400'
                                >
                                    api.elaxan.com
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            )}
            {/* search function */}
            <Search
                currentLanguage={currentLanguage}
                listCategory={listCategory}
                selectedCategory={selectedCategory}
                setSelectedCategory={setSelectedCategory}
                searchTerm={searchTerm}
                currentType={currentType}
                setCurrentType={setCurrentType}
                setSearchTerm={setSearchTerm}
                loadGI={loadGI}
                loadSR={loadSR}
                setLoading={setLoading}
                handleSearch={setSearchInputValue}
                setShowCommands={setShowCommands}
                fetchOnSearch={fetchOnSearch}
                setFetchOnSearch={setFetchOnSearch}
                setShowImage={setShowImage}
                showCommands={showCommands}
                showImage={showImage}
            />
            {/* end search function */}

            {/* Show Data List */}
            {!error && (
                <>
                    {currentType === 'Genshin Impact' ? (
                        <DataCard
                            code={cookies.code}
                            uid={cookies.uid}
                            server={cookies.server}
                            currentLanguage={currentLanguage}
                            filteredData={filteredData}
                            currentLimit={currentLimit}
                            showCommands={showCommands}
                            showImage={showImage}
                        />
                    ) : (
                        <DataCardSR
                            showImage={showImage}
                            currentLanguage={currentLanguage}
                            data={filteredDataSR}
                            currentLimit={currentLimit}
                        />
                    )}
                </>
            )}
            {/* End of Show Data List */}

            {/* Load More Button */}
            {!error &&
                currentLimit <
                    (currentType === 'Genshin Impact'
                        ? filteredData
                        : filteredDataSR
                    ).length && (
                    <div className='flex justify-center pt-5'>
                        <button
                            className='w-full rounded-2xl border-2 bg-cyan-700 p-4 duration-300 hover:border-cyan-500 hover:transition-colors sm:w-1/2 md:w-2/5 lg:w-[48%]'
                            onClick={() => {
                                setCurrentLimit(currentLimit + 100);
                            }}
                        >
                            Load More
                        </button>
                    </div>
                )}
            {/* End of Load More Button */}

            {/* Handle message for Fetch On Search without close button */}
            {fetchOnSearch &&
                (currentType === 'Genshin Impact'
                    ? filteredData
                    : filteredDataSR
                ).length === 0 && (
                    <div className='my-20 flex w-full items-center justify-center'>
                        <div className='text-center'>
                            <h1 className='text-3xl font-bold text-gray-300'>
                                Search for something.
                            </h1>
                            <p className='flex text-gray-400'>
                                Search for an item by typing in the search bar
                                above.
                            </p>
                        </div>
                    </div>
                )}
            {/* End of Handle message for Fetch On Search */}

            {!error && noResult && (
                <div className='my-20 flex w-full items-center justify-center'>
                    <div className='text-center'>
                        <h1 className='text-3xl font-bold text-gray-300'>
                            No results found.
                        </h1>
                        <p className='flex text-gray-400'>
                            No result were found for your query:
                            <span className='ml-1 text-base font-bold'>
                                {searchInputValue}
                            </span>
                        </p>
                    </div>
                </div>
            )}

            {/* Error */}
            {error && (
                <div className='flex h-full w-full items-center justify-center'>
                    <div className='text-center'>
                        <h1 className='text-3xl font-bold text-red-500'>
                            An error occurred.
                        </h1>
                        <span className='flex text-red-400'>
                            Error message:{' '}
                            <p className='ml-1 text-base font-bold'>
                                {errorMessage}
                            </p>
                        </span>
                    </div>
                </div>
            )}
            {/* End of Error */}

            {/* Loading */}
            {loading && (
                <div className='flex h-60 w-full items-center justify-center p-5'>
                    <span className='loading loading-dots w-[70px]'></span>
                </div>
            )}
            {/* End of Loading */}
            <div className='m-4'></div>
            <Footer />
            <ToastContainer />
        </div>
    );
}

export default App;
